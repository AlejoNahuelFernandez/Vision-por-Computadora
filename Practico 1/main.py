import random

def adivinar(cantidadIntentos):
    numero = random.randint(0, 100)
    print('Numero: ', numero)
    intentos = 0

    while(True):
        estimacion = input('Ingrese un numero : ')
        estimacion = int(estimacion)
        intentos = intentos + 1
        if estimacion == numero:
            print('Adivinaste')
            print('Cantidad de intentos: ', intentos)
            break

        if intentos > cantidadIntentos:
            print('Se ha superado la cantidad de intentos')
            break

cantidadDeIntentos = 5
adivinar(cantidadDeIntentos)


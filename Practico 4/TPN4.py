# IMPORTACIÓN LIBRERÍAS

import cv2

# DEFINICIÓN VARIABLE

cap = cv2.VideoCapture (0)

fourcc = cv2.VideoWriter_fourcc ('X', 'V', 'I', 'D')

fps = cap.get (cv2.CAP_PROP_FPS)

width = cap.get (cv2.CAP_PROP_FRAME_WIDTH)

height = cap.get (cv2.CAP_PROP_FRAME_HEIGHT)

out = cv2.VideoWriter ('output.avi', fourcc, int (fps), (int (width), int (height)))

# RECORRIDO CÁMARA

while cap.isOpened ():

    ret, frame = cap.read ()

    if ret is True:

        out.write (frame)

        # MUESTREO IMÁGENES

        cv2.imshow ('frame', frame)

        if cv2.waitKey (1) & 0xFF == ord ('q'):

            break

    else:

        break

# LIBERACIÓN VARIABLE

cap.release ()

# LIBERACIÓN SALIDA

out.release ()

# DESTRUCCIÓN PANTALLAS

cv2.destroyAllWindows ()
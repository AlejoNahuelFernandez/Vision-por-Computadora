# IMPORTACIÓN LIBRERÍAS

import numpy as np
import cv2
import pylab as pl
from matplotlib import pyplot as plt

# DEFINICIÓN FUNCIÓN

def similaridad (imagen, tx, ty, angle, s):

    (h, w) = (imagen.shape [0], imagen.shape [1])

    M = np.float32 ([[1, 0, tx], [0, 1, ty]])

    img_trasl = cv2.warpAffine (imagen, M, (w, h))

    (h, w) = img_trasl.shape [:2]

    M = cv2.getRotationMatrix2D ((w/2, h/2), angle, s)

    img_trasl_rot = cv2.warpAffine (img_trasl, M, (w, h))

    return img_trasl_rot

# LECTURA IMÁGENES

imagen = cv2.imread ('Pegatina.jpg')

# TRANSFORMACIÓN IMÁGENES

img_transf = similaridad (imagen, 30, -200, 45, 1.5)

pl.gray (), pl.axis ('equal'), pl.axis ('off')

# PASAJE BGR

plt.subplot (121), plt.imshow (imagen[:, :, ::-1]), plt.title ('Input')

plt.subplot (122), plt.imshow (img_transf[:, :, ::-1]), plt.title ('Output')

# MUESTREO IMÁGENES

plt.show ()
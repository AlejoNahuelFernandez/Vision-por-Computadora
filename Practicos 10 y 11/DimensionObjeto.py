# IMPORTACIÓN LIBRERÍAS

import cv2
import cv2.aruco
import numpy as np
from DeteccionObjeto import *

# CREACIÓN MARCADOR ARUCO

parametros = cv2.aruco.DetectorParameters_create ()

diccionario = cv2.aruco.Dictionary_get (cv2.aruco.DICT_5X5_50)

# CREACIÓN DETECTORES

detector = DetectorHomogeneo ()

# LECTURA CÁMARA

cap = cv2.VideoCapture (0)

cap.set (cv2.CAP_PROP_FRAME_WIDTH, 1280)

cap.set (cv2.CAP_PROP_FRAME_HEIGHT, 720)

# RECORRIDO CÁMARA

while True:

    # LECTURA CÁMARA

    _, imagen = cap.read ()

    # DETECCIÓN ESQUINAS

    esquinas, _, _ = cv2.aruco.detectMarkers (imagen, diccionario, parameters = parametros)

    if esquinas:

        # MUESTREO CONTORNOS MARCADOR ARUCO

        esquinas_entero = np.int0 (esquinas)

        cv2.polylines (imagen, esquinas_entero, True, (0, 255, 0), 5)

        # MUESTREO PERÍMETROS

        perimetro = cv2.arcLength (esquinas [0], True)

        # MUESTREO MEDIDAS EN UNIDAD REQUERIDA

        unidad_requerida = perimetro / 20

        # CREACIÓN CONTORNOS

        contornos = detector.DeteccionObjetos (imagen)

        # RECORRIDO CONTORNOS

        for contorno in contornos:

            # MUESTREO RECTIFICACIÓN

            rectificacion = cv2.minAreaRect (contorno)

            (x, y), (w, h), angle = rectificacion

            # MUESTREO MEDIDAS EN UNIDAD REQUERIDA

            ancho_objeto = w / unidad_requerida

            alto_objeto = h /  unidad_requerida

            # MUESTREO CAJA PUNTOS

            caja = cv2.boxPoints (rectificacion)

            caja = np.int0 (caja)

            # MUESTREO CONTORNOS

            cv2.circle (imagen, (int (x), int (y)), 5, (0, 0, 255), -1)

            cv2.polylines (imagen, [caja], True, (255, 0, 0), 2)

            # MUESTREO TEXTO

            cv2.putText (imagen, 'Ancho {} cm'.format (round (ancho_objeto, 1)), (int (x - 100), int (y - 20)), cv2.FONT_HERSHEY_PLAIN, 2, (100, 200, 0), 2)

            cv2.putText (imagen, 'Alto {} cm'.format (round (alto_objeto, 1)), (int (x - 100), int (y + 15)), cv2.FONT_HERSHEY_PLAIN, 2, (100, 200, 0), 2)

    # MUESTREO IMÁGENES

    cv2.imshow ('Imagen', imagen)

    clave = cv2.waitKey (1)

    if clave == 27:

        break

# LIBERACIÓN VARIABLE

cap.release ()

# DESTRUCCIÓN PANTALLAS

cv2.destroyAllWindows ()
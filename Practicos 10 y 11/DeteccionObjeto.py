# IMPORTACIÓN LIBRERÍAS

import cv2

# DEFINICIÓN CLASES

class DetectorHomogeneo ():

    # DEFINICIÓN FUNCIONES

    def __init__ (self):

        pass

    # DEFINICIÓN FUNCIONES

    def DeteccionObjetos (self, frame):

        # CONVERSIÓN ESCALA GRISES

        gris = cv2.cvtColor (frame, cv2.COLOR_BGR2GRAY)

        # CREACIÓN MÁSCARAS

        mascara = cv2.adaptiveThreshold (gris, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY_INV, 19, 5)

        # CREACIÓN CONTORNOS

        contornos, _ = cv2.findContours (mascara, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

        contornos_objeto = []

        # RECORRIDO CONTORNOS

        for contorno in contornos:

            # CREACIÓN ÁREA

            area = cv2.contourArea (contorno)

            if area > 2000:

                contornos_objeto.append (contorno)

        return contornos_objeto
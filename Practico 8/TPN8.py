# IMPORTACIÓN LIBRERÍAS

import cv2
import numpy as np
import pylab as pl

from matplotlib import pyplot as plt

# DEFINICIÓN FUNCIÓN

def inicio_datos ():

    global img_base, pts, img_agr, denominacion_img_base

    print ("Número de Imagen Base = ")

    denominacion_img_base = input ()

    print ("Número de Imagen Agregable = ")

    nombre_img_agregable = input ()

    pts = []

    # LECTURA IMÁGENES

    img_base = cv2.imread (f'{denominacion_img_base}.jpg')

    img_agr = cv2.imread (f'{nombre_img_agregable}.jpg')

# DEFINICIÓN FUNCIÓN

def agr_punto (event, x, y, flags, param):

    global img_base

    if event == cv2.EVENT_LBUTTONDOWN:

        cv2.circle (img_base, (x, y), 1, (0, 0, 0), 3)

        pts.append ([x, y])

# DEFINICIÓN FUNCIÓN

def img_transf_afin (img, pts_destino, ancho_final, alto_final):

    alto, ancho = img.shape [0], img.shape [1]

    pts_origen = np.float32 ([[0, 0], [ancho, 0], [0, alto]])

    M = cv2.getAffineTransform (pts_origen, pts_destino)

    img_transformada_afin = cv2.warpAffine (img_agr, M, (ancho_final, alto_final))

    # ESCRITURA IMÁGENES

    cv2.imwrite ('img_transformada_afin.jpg', img_transformada_afin)

    return img_transformada_afin

# DEFINICIÓN FUNCIÓN

def img_compuesta (img_base, img_agregable):

    umbral = [0, 0, 0]

    img_resultado = img_agregable.copy ()

    # RECORRIDO IMÁGENES

    for x in range (img_resultado.shape [1]):

        for y in range (img_resultado.shape [0]):

            if (img_resultado [y][x] == umbral).all ():

                img_resultado [y][x] = img_base [y][x]

    return img_resultado

# DEFINICIÓN FUNCIÓN

def mostrar_resultado ():

    # PASAJE BGR

    pl.gray (), pl.axis ('equal'), pl.axis ('off')

    plt.subplot (121), plt.imshow (img_base [:, :, ::-1]), plt.title ('Input')

    plt.subplot (122), plt.imshow (img_resultado [:, :, ::-1]), plt.title ('Output')

    # MUESTREO IMÁGENES

    plt.show ()

# LLAMADO FUNCIÓN

inicio_datos ()

cv2.namedWindow ('image')

cv2.setMouseCallback ('image', agr_punto)

# RECORRIDO IMÁGENES

while (1):

    # MUESTREO IMÁGENES

    cv2.imshow ('image', img_base)

    k = cv2.waitKey (1) & 0xFF

    if k == ord ('g'):

        if len (pts) == 3:

            pts_destino = np.float32 ([pts])

            img_transformada_afin = img_transf_afin (img_agr, pts_destino, img_base.shape [1], img_base.shape [0])

            # ESCRITURA IMÁGENES

            cv2.imwrite ('img_transformada_afin.jpg', img_transformada_afin)

            img_resultado = img_compuesta (img_base, img_transformada_afin)

            # ESCRITURA IMÁGENES

            cv2.imwrite ('img_resultado.jpg', img_resultado)

            break

    elif k == ord ('r'):

        # LECTURA IMÁGENES

        img_base = cv2.imread (f'{denominacion_img_base}.jpg')

        pts = []

    elif (k == 27 or k == ord ('q')):

        break

# DESTRUCCIÓN PANTALLAS

cv2.destroyAllWindows ()

# LLAMADO FUNCIÓN

mostrar_resultado ()
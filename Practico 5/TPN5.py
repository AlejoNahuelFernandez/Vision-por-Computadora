# IMPORTACIÓN LIBRERÍAS

import cv2

# DEFINICIÓN VARIABLE

drawing = False

# DEFINICIÓN PUNTOS FINALES

ix, iy = -1, -1
fx, fy = -1, -1

# DEFINICIÓN FUNCIÓN

def draw_rectangle (event, x, y, flags, param):

    global ix, iy, drawing, fx, fy, img

    if event == cv2.EVENT_LBUTTONDOWN:

        drawing = True

        ix, iy = x, y

    elif event == cv2.EVENT_MOUSEMOVE:

        if drawing is True:

            cv2.rectangle (img, (ix, iy), (x, y), (235, 235, 235), -1)

    elif event == cv2.EVENT_LBUTTONUP:

        drawing = False

        fx, fy = x, y

        # LECTURA IMÁGENES

        img = cv2.imread ('Pegatina.jpg')

        cv2.rectangle (img, (ix, iy), (x, y), (0, 0, 255), 2)

# LECTURA IMÁGENES

img = cv2.imread ('Pegatina.jpg')

# NOMBRAMIENTO IMÁGENES

cv2.namedWindow ('image')

# FUNCIÓN MOUSE

cv2.setMouseCallback ('image', draw_rectangle)

# RECORRIDO IMAGEN

while (1):

    # MUESTREO IMÁGENES

    cv2.imshow ('image', img)

    k = cv2.waitKey (1) & 0xFF

    if k == ord ('g'):

        imgToSave = img [iy+2:fy-2, ix+2:fx-2]

        # ESCRITURA IMÁGENES

        cv2.imwrite ('imgResultado.jpg', imgToSave)

        continue

    elif k == ord ('r'):

        # LECTURA IMÁGENES

        img = cv2.imread ('Pegatina.jpg')

    elif (k == 27 or k == ord ('q')):

        break

# DESTRUCCIÓN PANTALLAS

cv2.destroyAllWindows ()
# IMPORTACIÓN IMÁGENES

import cv2

# LECTURA IMÁGENES

img = cv2.imread ('hoja.png', 0)

# RECORRIDO IMÁGENES

for row in range (0, len (img)):

    for col in range (0, len (img [row])):

        if (img [row][col] < 250):

            img [row][col] = 0

        else:

            img [row][col] = 255

# ESCRITURA IMÁGENES

cv2.imwrite ('resultado.png', img)
# IMPORTACIÓN LIBRERÍAS

import numpy as np
import cv2
import pylab as pl
from matplotlib import pyplot as plt

# DEFINICIÓN FUNCIÓN

def translate_rotate (imagen, tx, ty, angle):

    (h, w) = (imagen.shape [0], imagen.shape [1])

    M = np.float32 ([[1, 0, tx], [0, 1, ty]])

    img_trasl = cv2.warpAffine (imagen, M, (w, h))

    (h, w) = img_trasl.shape [:2]

    M = cv2.getRotationMatrix2D ((w/2, h/2), angle, 1.0)

    img_traslyrot = cv2.warpAffine (img_trasl, M, (w, h))

    return img_traslyrot

# LECTURA IMÁGENES

img = cv2.imread ('Pegatina.jpg')

# TRASLACIÓN Y ROTACIÓN IMÁGENES

img_traslyrot = translate_rotate (img, 10, 20, 30)

pl.gray (), pl.axis ('equal'), pl.axis ('off')

# PASAJE BGR

plt.subplot (121), plt.imshow (img [:, :, ::-1]), plt.title ('Input')

plt.subplot (122), plt.imshow (img_traslyrot [:, :, ::-1]), plt.title ('Output')

# MUESTREO IMÁGENES

plt.show ()
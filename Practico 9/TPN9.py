# IMPORTACIÓN LIBRERÍAS

import cv2
import numpy as np
import pylab as pl

from matplotlib import pyplot as plt

# DEFINICIÓN FUNCIÓN

def inicia_datos ():

    global img_base, pts, img_agregar, nombre_img_base, alto_ingresado, ancho_ingresado

    print ("Número Imagen Base = ")

    nombre_img_base = input ()

    print ("Alto Imagen Resultante = ")

    alto_ingresado = int (input ())

    print ("Ancho Imagen Resultante = ")

    ancho_ingresado = int (input ())

    pts = []

    # LECTURA IMÁGENES

    img_base = cv2.imread (f'{nombre_img_base}.jpg')

# DEFINICIÓN FUNCIÓN

def agrego_punto (event, x, y, flags, param):

    if event == cv2.EVENT_LBUTTONDOWN:

        cv2.circle (img_base, (x, y), 1, (0, 0, 0), 3)

        pts.append ([x, y])

# DEFINICIÓN FUNCIÓN

def img_transf_perspectiva (img, pts_destino, ancho_final, alto_final):

    alto, ancho = img.shape [0], img.shape [1]

    pts_origen = np.float32 ([[0, 0], [ancho, 0], [0, alto], [ancho, alto]])

    M = cv2.getPerspectiveTransform (pts_origen, pts_destino)

    img_transf_perspectiva = cv2.warpPerspective (img, M, (ancho_final, alto_final))

    return img_transf_perspectiva

# DEFINICIÓN FUNCIÓN

def imag_blanca (ancho, largo):

    img_blanca = np.zeros ((ancho, largo, 3), np.uint8)

    img_blanca [:] = 255

    # ESCRITURA IMÁGENES

    cv2.imwrite ('img_blanca.jpg', img_blanca)

    return img_blanca

# DEFINICIÓN FUNCIÓN

def rectificar ():

    img_blanca = imag_blanca (alto_ingresado, ancho_ingresado)

    pts_seleccionados = np.float32 ([pts])

    ancho_final, alto_final = img_base.shape [1], img_base.shape [0]

    img_blanca_persp = img_transf_perspectiva (img_blanca, pts_seleccionados, ancho_final, alto_final)

    # ESCRITURA IMÁGENES

    cv2.imwrite ('img_blanca_perspectiva.jpg', img_blanca_persp)

    umbral = [255, 255, 255]

    img_seleccio_persp = img_blanca_persp.copy ()

    # RECORRIDO IMÁGENES

    for x in range (img_seleccio_persp.shape [1]):

        for y in range (img_seleccio_persp.shape [0]):

            if (img_seleccio_persp [y][x] == umbral).all ():

                img_seleccio_persp [y][x] = img_base [y][x]

    # ESCRITURA IMÁGENES

    cv2.imwrite ('img_seleccionada_perspectiva.jpg', img_seleccio_persp)

    pts_origen2 = np.float32 ([pts])

    pts_destino2 = np.float32 ([[0, 0], [ancho_ingresado, 0], [0, alto_ingresado], [ancho_ingresado, alto_ingresado]])

    M = cv2.getPerspectiveTransform (pts_origen2, pts_destino2)

    global img_seleccio_rectif

    img_seleccio_rectif = cv2.warpPerspective (img_seleccio_persp, M, (ancho_ingresado, alto_ingresado))

    # ESCRITURA IMÁGENES

    cv2.imwrite ('img_seleccionada_rectificada.jpg', img_seleccio_rectif)

# DEFINICIÓN FUNCIÓN

def mostrar_resultado ():

    # PASAJE BGR
    pl.gray (), pl.axis ('equal'), pl.axis ('off')

    plt.subplot (121), plt.imshow (img_base [:, :, ::-1]), plt.title ('Input')

    plt.subplot (122), plt.imshow (img_seleccio_rectif [:, :, ::-1]), plt.title ('Output')

    # MUESTREO IMÁGENES

    plt.show ()

# LLAMADO FUNCIÓN

inicia_datos ()

cv2.namedWindow ('image')

cv2.setMouseCallback ('image', agrego_punto)

# RECORRIDO IMÁGENES

while (1):

    # MUESTREO IMÁGENES

    cv2.imshow ('image', img_base)

    k = cv2.waitKey (1) & 0xFF

    if k == ord ('g'):

        if len (pts) == 4:

            pts_destino = np.float32 ([pts])

            # LLAMADO FUNCIÓN

            rectificar ()

            break

    elif k == ord ('r'):

        # LECTURA IMÁGENES

        img_base = cv2.imread (f'{nombre_img_base}.jpg')

        pts = []

    elif (k == 27 or k == ord ('q')):

        break

# DESTRUCCIÓN PANTALLAS

cv2.destroyAllWindows()

# LLAMADO FUNCIÓN

mostrar_resultado()
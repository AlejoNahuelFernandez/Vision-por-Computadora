import numpy as np
import cv2

MIN_MATCH_COUNT = 10

imagen1 = cv2.imread('imagen1.jpg')
img1 = cv2.resize(imagen1, (600,800))
imagen2 = cv2.imread('imagen2.jpg')
img2 = cv2.resize(imagen2, (600,800))

dscr = cv2.SIFT_create()

kp1, des1 = dscr.detectAndCompute(img1, None)
kp2, des2 = dscr.detectAndCompute(img2, None)

matcher = cv2.BFMatcher(cv2.NORM_L2)

matches = matcher.knnMatch(des1, des2, k=2)

#Guardamos los buenos matches usando el test de razón de Lowe
good = []
for m, n in matches:
    if m.distance < 0.7*n.distance:
        good.append(m)

if (len(good) > MIN_MATCH_COUNT):
    src_pts = np.float32([kp1[m. queryIdx].pt for m in good]).reshape(-1 , 1 , 2)
    dst_pts = np.float32 ([kp2[m.trainIdx].pt for m in good]).reshape(-1 , 1 , 2)
    #Computamos la homografía con RANSAC
    H, mask = cv2.findHomography(dst_pts, src_pts, cv2.RANSAC, 5.0)

#Aplicamos la transformación perspectiva H a la imagen 2
wimg2 = cv2.warpPerspective(img2, H, (600,800))

#Mezclamos ambas imágenes
alpha = 0.5
mezcla = np.array(wimg2 * alpha + img1 * (1 - alpha), dtype=np . uint8)

#Resultados
imagenConMatches = cv2.drawMatches(img1,kp1,img2,kp2,good,None,flags=2)
cv2.imwrite('matches.png', imagenConMatches)
cv2.imwrite('resultado.png', mezcla)



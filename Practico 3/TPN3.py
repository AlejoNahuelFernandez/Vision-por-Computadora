# IMPORTACIÓN LIBRERÍAS

import time
import cv2

# DEFINICIÓN VARIABLE

cap = cv2.VideoCapture (0)

fps = cap.get (cv2.CAP_PROP_FPS)

#  ESCRITURA POR PANTALLA

print ('FPS = {0}' .format (fps))

# DEFINICIÓN CONSTANTE

numero_frames = 120;

#  ESCRITURA POR PANTALLA

print ('Frames = {0}' .format (numero_frames))

# ARRANQUE TIEMPO

arranque = time.time ()

# GRABACIÓN PANTALLA

for i in range (0, numero_frames):

    ret, frame = cap.read ()

# FINALIZACIÓN TIEMPO

finalizacion = time.time ()

# TIEMPO TRANSCURRIDO

segundos = finalizacion - arranque

#  ESCRITURA POR PANTALLA

print ('Tiempo transcurrido = {0} segundos' .format (segundos))

# FRAMES POR SEGUNDO

fps = numero_frames / segundos

#  ESCRITURA POR PANTALLA

print ('FPS = {0}' .format (fps))

# LIBERACIÓN VARIABLE

cap.release ()

# DESTRUCCIÓN PANTALLAS

cv2.destroyAllWindows ()